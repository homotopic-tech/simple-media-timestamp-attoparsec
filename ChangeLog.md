# Changelog for simple-media-timestamp-attoparsec

## v0.1.0.0

* Add parsers for ffmpeg's timestamp format and srt's timestamp format.
